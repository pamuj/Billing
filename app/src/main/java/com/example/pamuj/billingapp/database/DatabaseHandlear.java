package com.example.pamuj.billingapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.renderscript.Sampler;
import android.util.Log;

import java.util.List;

/**
 * Created by pamuj on 10-08-2017.
 */

public class DatabaseHandlear extends SQLiteOpenHelper {

    private static DatabaseHandlear mInstance;
    private final Context myContext;
    private SQLiteDatabase myWritableDb;

    public DatabaseHandlear(Context context) { // in constroctor we are creating database DB_LBOOK
        super(context, "DB_BILL", null, 1);
        this.myContext = context;
    }

    public static DatabaseHandlear getInstance(Context context) { // this method create instance for our database class
        if (mInstance == null) {
            mInstance = new DatabaseHandlear(context);
        }
        return mInstance;
    }

    public SQLiteDatabase getMyWritableDatabase() { // geting permission to write database
        if ((myWritableDb == null) || (!myWritableDb.isOpen())) {
            myWritableDb = this.getWritableDatabase();
        }

        return myWritableDb;
    }

    @Override
    public void close() { // releasing permission of writing data base
        super.close();
        if (myWritableDb != null) {
            myWritableDb.close();
            myWritableDb = null;
        }
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) { // in this method table will create
        sqLiteDatabase.execSQL(BillDetails.SQL); // this account of doctor lets show you

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) { // this is to when you want to chnage in database you can drop all table
        sqLiteDatabase.execSQL("drop table "+BillDetails.TableName);

        onCreate(sqLiteDatabase); // create again for next version
    }

    public void insertData(List<String> nameOFColoums,List<String> dataForColums,String tableName){ // this method created by me for inserting data it can be used by any table
        // in my database
        if(nameOFColoums.size()==dataForColums.size()) {
            SQLiteDatabase db=this.getMyWritableDatabase();
            ContentValues contentValues=new ContentValues();
            for (int i = 0; i < nameOFColoums.size(); i++) {
                contentValues.put(nameOFColoums.get(i),dataForColums.get(i));
            }
            db.insert(tableName,null,contentValues);
            Log.i("LBOOK---",tableName+" data inserted");
        }
    }
    public void deletion(String table,String idName,String connetingId,String[] stringsValues){  // similarly delection
        SQLiteDatabase db=this.getMyWritableDatabase();
        db.delete(table,idName+"=? and "+connetingId+"=?",stringsValues);
        Log.i("LBOOK---",table+" data deleted");
    }

    public Cursor getDataFromTable(String table,String id,String valueOfId){
        String where="";
        if(!id.equals("")){
           where="where "+id+"=  \'"+valueOfId+"\'";
        }
        SQLiteDatabase db=this.getMyWritableDatabase();
        return db.rawQuery("Select * from "+table+" "+where,null);

    }

    public void updateDataRaw(List<String> nameOFColoums,List<String> dataForColums,String tableName,String id,String idValue){ //updation
        if(nameOFColoums.size()==dataForColums.size()) {
            SQLiteDatabase db=this.getMyWritableDatabase();
            ContentValues contentValues=new ContentValues();
            for (int i = 0; i < nameOFColoums.size(); i++) {
                contentValues.put(nameOFColoums.get(i),dataForColums.get(i));
            }
            db.update(tableName,contentValues,id+"="+idValue,null);
            Log.i("LBOOK---",tableName+" data updated");
        }
    }
    public void tableAlter(){ // alter table
        SQLiteDatabase db=this.getMyWritableDatabase();
      //  db.execSQL("alter table "+PaitentEntryTable.TableName+" add column "+PaitentEntryTable.Images+" varchar");
    }


}
