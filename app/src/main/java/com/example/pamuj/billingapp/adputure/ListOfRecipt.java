package com.example.pamuj.billingapp.adputure;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.example.pamuj.billingapp.R;
import com.example.pamuj.billingapp.database.BillDetails;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pamuj on 21-09-2017.
 */

public class ListOfRecipt extends BaseAdapter {
    LayoutInflater inflater;
    Context context;
    Cursor listOfName;
    public ListOfRecipt(Context context, Cursor stringList){
        listOfName=stringList;
        listOfName.moveToFirst();
        this.context=context;
        inflater=LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return listOfName.getCount();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view=inflater.inflate(R.layout.list_recipt,null,true);
       // Typeface myCustomFontLight= VariableHelper.typeface(context);
        //TextView textName=(TextView)view.findViewById(R.id.textName);
        ListView listView=(ListView)view.findViewById(R.id.list_item);
       // listView.setAdapter(new ArrayAdapter<String>();

        listOfName.moveToNext();

        return view;
    }
    public List<String> getList(String s){
        String[] array=s.split("DOT");
        List<String> list=new ArrayList<>();
        for(int i=0;i<array.length;i++){
            list.add(array[i]);
        }

        return list;
    }
}
