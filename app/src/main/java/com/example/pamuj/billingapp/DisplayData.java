package com.example.pamuj.billingapp;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.pamuj.billingapp.adputure.CustomExpandableListAdapter;
import com.example.pamuj.billingapp.database.BillDetails;
import com.example.pamuj.billingapp.database.DatabaseHandlear;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DisplayData extends AppCompatActivity {
    LinearLayout listView;
    TextView detailsOfSold;

    DatabaseHandlear db=DatabaseHandlear.getInstance(this);
    List<String> dates=new ArrayList<>();
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_data);
        listView=(LinearLayout) findViewById(R.id.linearLayout);
        detailsOfSold=(TextView) findViewById(R.id.detailsOfSold);
        spinner=(Spinner) findViewById(R.id.spinner);
        Cursor cursor=db.getDataFromTable(BillDetails.TableName,"","");
        cursor.moveToLast();
        HashMap<String,List<String>> hashMap=new HashMap<>();
        List<String> tagKeys=new ArrayList<>();
        for(int i=0;i<cursor.getCount();i++){
            if(!dates.contains(cursor.getString(cursor.getColumnIndex(BillDetails.Date)))){
                dates.add(cursor.getString(cursor.getColumnIndex(BillDetails.Date)));
            }
            tagKeys.add("("+i+") Bill Detail : "+cursor.getString(cursor.getColumnIndex(BillDetails.Date)));
            hashMap.put("("+i+") Bill Detail : "+cursor.getString(cursor.getColumnIndex(BillDetails.Date)),getList(cursor.getString(cursor.getColumnIndex(BillDetails.Reciept))));

            cursor.moveToPrevious();
        }
        addView(tagKeys,hashMap);

        spinner.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,dates));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int teaT=0,coffeeT=0,dossaT=0,shakeT=0,coldDrinkT=0,breadT=0;
                Cursor cursor=db.getDataFromTable(BillDetails.TableName,BillDetails.Date,spinner.getSelectedItem().toString());
                cursor.moveToLast();
                for(int ii=0;ii<cursor.getCount();ii++){
                    teaT=teaT+Integer.parseInt(cursor.getString(cursor.getColumnIndex(BillDetails.ItemTeaQ)));
                    coffeeT=coffeeT+Integer.parseInt(cursor.getString(cursor.getColumnIndex(BillDetails.ItemCoffeeQ)));
                    shakeT=shakeT+Integer.parseInt(cursor.getString(cursor.getColumnIndex(BillDetails.ItemShakeQ)));
                    dossaT=dossaT+Integer.parseInt(cursor.getString(cursor.getColumnIndex(BillDetails.ItemDossaQ)));
                    coldDrinkT=coldDrinkT+Integer.parseInt(cursor.getString(cursor.getColumnIndex(BillDetails.ItemColdDrinkQ)));
                    breadT=breadT+Integer.parseInt(cursor.getString(cursor.getColumnIndex(BillDetails.ItemBreadQ)));
                    cursor.moveToPrevious();
                }
                detailsOfSold.setText("Total tea : "+teaT+" ,coffee : "+coffeeT+", dossa : "+dossaT+", shake : "+shakeT+", Cold Drink : "+coldDrinkT+", bread : "+breadT+"");

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    public void addView(List<String> keys ,HashMap<String,List<String>> hashMap){
       /* List<String> strings=new ArrayList<>();
        strings.add("Bill Detail :"+date);
        HashMap<String,List<String>> hs=new HashMap<>();
        hs.put("Bill Detail :"+date,getList(s));*/

        final View childView=getLayoutInflater().inflate(R.layout.list_recipt,null);
        ExpandableListView expandableListView=(ExpandableListView) childView.findViewById(R.id.list_item);
        expandableListView.setAdapter(new CustomExpandableListAdapter(this,keys, hashMap));

        this.listView.addView(childView);
    }

    public List<String> getList(String s){
        String[] array=s.split("DOT");
        List<String> list=new ArrayList<>();
        for(int i=0;i<array.length;i++){
            list.add(array[i]);
        }

        return list;
    }
}
