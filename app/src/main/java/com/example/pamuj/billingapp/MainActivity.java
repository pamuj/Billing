package com.example.pamuj.billingapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pamuj.billingapp.database.BillDetails;
import com.example.pamuj.billingapp.database.DatabaseHandlear;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tvTea,tVCoffee,tvDossa,tvShake,tvColdDrink,Bread,tvCheckDetail,totalBill;
    LinearLayout linearLayout;
    int flagForItems;
    List<TextView> itemListDetail=new ArrayList<>();
    List<TextView> itemListAmount=new ArrayList<>();
    int[] itemRates;
    static int  totalAmount=0;
    int tea=0;
    int coffee=0;
    int dossa=0;
    int shake=0;
    int coldDrink=0;
    int bread=0;
    private PopupWindow pwindo;
    DatabaseHandlear db=DatabaseHandlear.getInstance(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linearLayout=(LinearLayout)findViewById(R.id.linearLayout);
        tvTea=(TextView)findViewById(R.id.tvTea);
        totalBill=(TextView)findViewById(R.id.totalBill);
        tvTea.setOnClickListener(this);
        tVCoffee=(TextView)findViewById(R.id.tVCoffee);
        tVCoffee.setOnClickListener(this);
        tvDossa=(TextView)findViewById(R.id.tvDossa);
        tvDossa.setOnClickListener(this);
        tvShake=(TextView)findViewById(R.id.tvShake);
        tvShake.setOnClickListener(this);
        tvColdDrink=(TextView)findViewById(R.id.tvColdDrink);
        tvColdDrink.setOnClickListener(this);
        Bread=(TextView)findViewById(R.id.Bread);
        Bread.setOnClickListener(this);
        tvCheckDetail=(TextView)findViewById(R.id.tvCheckDetail);
        tvCheckDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO start new activity
                startActivity(new Intent(MainActivity.this,DisplayData.class));
            }
        });
        totalBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                genrateBill();
            }
        });
        itemRates=getResources().getIntArray(R.array.itemRate);

    }

    private void genrateBill() {
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.genrate_bill,
                (ViewGroup) findViewById(R.id.pop));
        pwindo = new PopupWindow(layout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, true);
        pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);
        ImageView back=(ImageView)layout.findViewById(R.id.back);
        ListView listView=(ListView) layout.findViewById(R.id.list_view);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pwindo.dismiss();
            }
        });
        List<String> strings=new ArrayList<>();

        strings.add("Date : "+dateCreate());
        strings.add("--------------------------");
        for(int i=0;i<itemListAmount.size();i++){
            strings.add(" ("+i+") "+itemListDetail.get(i).getText()+" :"+itemListAmount.get(i).getText().toString());
        }
        strings.add("--------------------------");
        strings.add("Amount               : "+totalAmount);
        listView.setAdapter(new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_list_item_1,strings));

        List<String> columnName=new ArrayList<>();
        List<String> columnValue=new ArrayList<>();
        columnName.add(BillDetails.Date);
        columnName.add(BillDetails.ItemTeaQ);
        columnName.add(BillDetails.ItemCoffeeQ);
        columnName.add(BillDetails.ItemDossaQ);
        columnName.add(BillDetails.ItemShakeQ);
        columnName.add(BillDetails.ItemColdDrinkQ);
        columnName.add(BillDetails.ItemBreadQ);
        columnName.add(BillDetails.TotalBill);
        columnName.add(BillDetails.Reciept);

        // add values
        columnValue.add(dateCreate());
        columnValue.add(tea+"");
        columnValue.add(coffee+"");
        columnValue.add(dossa+"");
        columnValue.add(shake+"");
        columnValue.add(coldDrink+"");
        columnValue.add(bread+"");
        columnValue.add(totalAmount+"");
        String valuesRec="";
        for(int i=0;i<strings.size();i++){
            valuesRec=valuesRec+strings.get(i)+"DOT";
        }
        columnValue.add(valuesRec);
        db.insertData(columnName,columnValue,BillDetails.TableName);

        Toast.makeText(this, "Bill Created", Toast.LENGTH_SHORT).show();
        linearLayout.removeAllViews();
        tea=0;
        coffee=0;
        dossa=0;
        shake=0;
        coldDrink=0;
        bread=0;
        totalAmount=0;
        itemListAmount.clear();
        itemListDetail.clear();


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tvTea:
                tea++;
                addView(0);
                break;
            case R.id.tVCoffee:
                coffee++;
                addView(1);
                break;
            case R.id.tvDossa:
                dossa++;
                addView(2);
                break;
            case R.id.tvShake:
                shake++;
                addView(3);
                break;
            case R.id.tvColdDrink:
                coldDrink++;
                addView(4);
                break;
            case R.id.Bread:
                bread++;
                addView(5);
                break;
        }
    }

    public void addView(final int prizeValue){
        final View childView=getLayoutInflater().inflate(R.layout.bill_item,null);
        final TextView itemDetails=(TextView)childView.findViewById(R.id.itemDetails);
        itemListDetail.add(itemDetails);
        switch (prizeValue){
            case 0:
                itemDetails.setText("Tea : ");
                break;
            case 1:
                itemDetails.setText("Coffee : ");
                break;
            case 2:
                itemDetails.setText("Dossa : ");
                break;
            case 3:
                itemDetails.setText("Shake : ");
                break;
            case 4:
                itemDetails.setText("Cold Drink : ");
                break;
            case 5:
                itemDetails.setText("Bread : ");
                break;
        }

        final TextView itemPrize=(TextView)childView.findViewById(R.id.itemPrize);
        itemListAmount.add(itemPrize);
        itemPrize.setText(itemRates[prizeValue]+"");

        TextView remove=(TextView)childView.findViewById(R.id.remove);

        final String prize =itemPrize.getText().toString();
        totalAmount=totalAmount+(Integer.parseInt(prize));
        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linearLayout.removeView(childView);
               totalAmount= totalAmount-(Integer.parseInt(prize));
                switch (prizeValue){
                    case 0:
                        tea--;
                        break;
                    case 1:
                        coffee--;
                        break;
                    case 2:
                        dossa--;
                        break;
                    case 3:
                        shake--;
                        break;
                    case 4:
                        coldDrink--;
                        break;
                    case 5:
                        bread--;
                        break;
                }
                if(itemListDetail.contains(itemDetails)){
                    itemListDetail.remove(itemDetails);
                }
                if(itemListAmount.contains(itemPrize)){
                    itemListAmount.remove(itemPrize);
                }
            }
        });
        linearLayout.addView(childView);
    }

    public static String dateCreate(){
        SimpleDateFormat currentDate = new SimpleDateFormat("dd/MM/yyyy");
        Date todayDate = new Date();
        return currentDate.format(todayDate);
    }
}
